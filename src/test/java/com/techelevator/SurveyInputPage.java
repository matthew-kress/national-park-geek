package com.techelevator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class SurveyInputPage {
	private WebDriver webDriver;
	
	public SurveyInputPage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public void selectPark(String park){
		WebElement selectPark = webDriver.findElement(By.id("parkName"));
		Select select = new Select(selectPark);
		select.selectByVisibleText(park);
	}
	
	public void inputEmail(String email) {
		WebElement emailInput = webDriver.findElement(By.id("emailAddress"));
		emailInput.sendKeys(email);
	}
	
	public void inputState(String state) {
		WebElement stateInput = webDriver.findElement(By.id("state"));
		stateInput.sendKeys(state);
	}
	
	public void selectActivityLevel(String activity) {
		WebElement selectActivityLevel = webDriver.findElement(By.id("activityLevel"));
		Select select = new Select(selectActivityLevel);
		select.deselectByVisibleText(activity);
	}
	
	public SurveyResultPage submit() {
		WebElement submitButton = webDriver.findElement(By.className("formSubmitButton"));
		submitButton.submit();
		return new SurveyResultPage(webDriver);
	}
	
	
	
	
}
