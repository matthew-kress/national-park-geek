package com.techelevator.npgeek.cukes.steps;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import com.techelevator.HomePage;
import com.techelevator.ParkDetailPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ParkDetailSteps {
	private WebDriver webDriver;
	private HomePage homePage;
	private ParkDetailPage parkDetailPage;
	
	@Autowired
	public ParkDetailSteps(WebDriver webDriver) {
		this.webDriver = webDriver;
		homePage = new HomePage(webDriver);
		parkDetailPage = new ParkDetailPage(webDriver);
	}
	
	@Given("^I am on the home page$")
	public void goToHomePage() throws Throwable {
		webDriver.get("http://localhost:8080/m3-java-capstone/");
	}
	
	@When("^I choose the park (.*)$")
	public void goToParkDetail(String parkCode) throws Throwable {
		homePage.clickParkDetailLink(parkCode);
	}
	
	@Then("^the name should be (.*)$")
	public void verifyName(String name) throws Throwable {
		parkDetailPage.verifyName(name);
		Assert.assertTrue(parkDetailPage.verifyName(name));
	}
	
	@Then("^the number of acres should be (.*)$")
	public void verifyAcres(String acres) throws Throwable {
		parkDetailPage.verifyAcres(acres);
		Assert.assertTrue(parkDetailPage.verifyAcres(acres));
	}
	
	@Then("^the elevation should be (.*)$")
	public void verifyElevation(String elevation) throws Throwable {
		parkDetailPage.verifyElevation(elevation);
		Assert.assertTrue(parkDetailPage.verifyElevation(elevation));
	}
	
	@Then("^the miles of trail should be (.*)$")
	public void verifyMiles(String miles) throws Throwable {
		parkDetailPage.verifyMilesOfTrail(miles);
		Assert.assertTrue(parkDetailPage.verifyMilesOfTrail(miles));
	}
	
	@Then("^the number of campsites should be (.*)$")
	public void verifyCampsites(String campsites) throws Throwable {
		parkDetailPage.verifyCampsites(campsites);
		Assert.assertTrue(parkDetailPage.verifyCampsites(campsites));
	}
	
	@Then("^the climate should be (.*)$")
	public void verifyClimate(String climate) throws Throwable {
		parkDetailPage.verifyClimate(climate);
		Assert.assertTrue(parkDetailPage.verifyClimate(climate));
	}
	
	@Then("^the year founded should be (.*)$")
	public void verifyYear(String year) throws Throwable {
		parkDetailPage.verifyYear(year);
		Assert.assertTrue(parkDetailPage.verifyYear(year));
	}
	
	@Then("^the visitor count should be (.*)$")
	public void verifyVisitors(String visitors) throws Throwable {
		parkDetailPage.verifyVisitorCount(visitors);
		Assert.assertTrue(parkDetailPage.verifyVisitorCount(visitors));
	}
	
	@Then("^the quote should be (.*)$")
	public void verifyQuote(String quote) throws Throwable {
		parkDetailPage.verifyQuote(quote);
		Assert.assertTrue(parkDetailPage.verifyQuote(quote));
	}
	
	@Then("^the quote source should be (.*)$")
	public void verifyQuoteSource(String quoteSource) throws Throwable {
		parkDetailPage.verifyQuoteSource(quoteSource);
		Assert.assertTrue(parkDetailPage.verifyQuoteSource(quoteSource));
	}
	
	@Then("^the description should be (.*)$")
	public void verifyDescription(String description) throws Throwable {
		parkDetailPage.verifyDescription(description);
		Assert.assertTrue(parkDetailPage.verifyDescription(description));
	}
	
	@Then("^the entry fee should be (.*)$")
	public void verifyEntryFee(String entryFee) throws Throwable {
		parkDetailPage.verifyEntryFee(entryFee);
		Assert.assertTrue(parkDetailPage.verifyEntryFee(entryFee));
	}
	
	@Then("^the number of animal species should be (.*)$")
	public void verifyAnimalSpecies(String animalSpecies) throws Throwable {
		parkDetailPage.verifyNumberOfAnimals(animalSpecies);
		Assert.assertTrue(parkDetailPage.verifyNumberOfAnimals(animalSpecies));
	}
	
	@Then("^the five day forecast should be (.*)$")
	public void verifyFiveDayForecast(String forecast) throws Throwable {
		parkDetailPage.verifyFiveDayForecast(forecast);
		Assert.assertTrue(parkDetailPage.verifyFiveDayForecast(forecast));
	}
	
	@Then("^the low should be (.*)$")
	public void verifyLow(String low) throws Throwable {
		parkDetailPage.verifyLow(low);
		Assert.assertTrue(parkDetailPage.verifyLow(low));
	}
	
	@Then("^the high should be (.*)$")
	public void verifyHigh(String high) throws Throwable {
		parkDetailPage.verifyHigh(high);
		Assert.assertTrue(parkDetailPage.verifyHigh(high));
	}
	
	@Then("^the recommendation should be (.*)$")
	public void verifyRecommendation(String recommendation) throws Throwable {
		parkDetailPage.verifyRecommendation(recommendation);
		Assert.assertTrue(parkDetailPage.verifyRecommendation(recommendation));
	}
	
}
