package com.techelevator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ParkDetailPage {
	private WebDriver webDriver;

	public ParkDetailPage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public boolean verifyName(String parkDetailTitle) {
		WebElement outputName = webDriver.findElement(By.id("parkDetailTitle"));
		return outputName.getText().equals(parkDetailTitle);
	}
	
	public boolean verifyAcres(String acres) {
		WebElement outputAcres = webDriver.findElement(By.id("acres"));
		return outputAcres.getText().equals(acres);
	}
	
	public boolean verifyElevation(String elevation) {
		WebElement outputElevation = webDriver.findElement(By.id("elevation"));
		return outputElevation.getText().equals(elevation);
	}
	
	public boolean verifyMilesOfTrail(String milesOfTrail) {
		WebElement outputMilesOfTrail = webDriver.findElement(By.id("milesOfTrail"));
		return outputMilesOfTrail.getText().equals(milesOfTrail);
	}
	
	public boolean verifyCampsites(String campsites) {
		WebElement outputCampsites = webDriver.findElement(By.id("campsites"));
		return outputCampsites.getText().equals(campsites);
	}
	
	public boolean verifyClimate(String climate) {
		WebElement outputClimate = webDriver.findElement(By.id("climate"));
		return outputClimate.getText().equals(climate);
	}
	
	public boolean verifyYear(String year) {
		WebElement outputYear = webDriver.findElement(By.id("year"));
		return outputYear.getText().equals(year);
	}
	
	public boolean verifyVisitorCount(String campsites) {
		WebElement outputCampsites = webDriver.findElement(By.id("visitorCount"));
		return outputCampsites.getText().equals(campsites);
	}
	
	public boolean verifyQuote(String quote) {
		WebElement outputQuote = webDriver.findElement(By.id("quote"));
		return outputQuote.getText().equals(quote);
	}
	
	public boolean verifyQuoteSource(String quoteSource) {
		WebElement outputQuoteSource = webDriver.findElement(By.id("quoteSource"));
		return outputQuoteSource.getText().equals(quoteSource);
	}
	
	public boolean verifyDescription(String description) {
		WebElement outputDescription = webDriver.findElement(By.id("description"));
		return outputDescription.getText().equals(description);
	}
	
	public boolean verifyEntryFee(String entryFee) {
		WebElement outputCampsites = webDriver.findElement(By.id("entryFee"));
		return outputCampsites.getText().equals(entryFee);
	}
	
	public boolean verifyNumberOfAnimals(String numberOfAnimals) {
		WebElement outputNumberOfAnimals = webDriver.findElement(By.id("numberOfAnimals"));
		return outputNumberOfAnimals.getText().equals(numberOfAnimals);
	}
	
	public boolean verifyFiveDayForecast(String fiveDayForecast) {
		WebElement outputFiveDayForecast = webDriver.findElement(By.id("fiveDayForecast"));
		return outputFiveDayForecast.getText().equals(fiveDayForecast);
	}
	
	public boolean verifyLow(String low) {
		WebElement outputLow = webDriver.findElement(By.id("low"));
		return outputLow.getText().equals(low);
	}
	
	public boolean verifyHigh(String high) {
		WebElement outputHigh = webDriver.findElement(By.id("high"));
		return outputHigh.getText().equals(high);
	}
	
	public boolean verifyRecommendation(String recommendation) {
		WebElement outputRecommendation = webDriver.findElement(By.id("recommendation"));
		return outputRecommendation.getText().equals(recommendation);
	}
	
}