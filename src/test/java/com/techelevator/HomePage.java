package com.techelevator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {
	private WebDriver webDriver;
	
	public HomePage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public ParkDetailPage clickParkDetailLink(String parkCode) {
		WebElement link = webDriver.findElement(By.id(parkCode));
		link.click();
		return new ParkDetailPage(webDriver);
	}
	
	public SurveyInputPage clickSurveyLink(){
		WebElement link = webDriver.findElement(By.id("surveyLink"));
		link.click();
		return new SurveyInputPage(webDriver);
	}
}