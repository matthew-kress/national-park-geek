Feature: Park Detail

Scenario: Test Park Detail Page First Park
Given I am on the home page
When I choose the park cvnp
Then the name should be Cuyahoga Valley National Park 
And the number of acres should be 32832
And the elevation should be 696
And the miles of trail should be 125.0
And the number of campsites should be 0
And the climate should be Woodland
And the year founded should be 2000
And the visitor count should be 2189849
And the quote should be Of all the paths you take in life, make sure a few of them are dirt.
And the quote source should be John Muir
And the description should be Though a short distance from the urban areas of Cleveland and Akron, Cuyahoga Valley National Park seems worlds away. The park is a refuge for native plants and wildlife, and provides routes of discovery for visitors. The winding Cuyahoga River gives way to deep forests, rolling hills, and open farmlands. Walk or ride the Towpath Trail to follow the historic route of the Ohio & Erie Canal
And the entry fee should be 0
And the number of animal species should be 390
And the five day forecast should be 1
And the low should be 38F
And the high should be 62F
And the recommendation should be Get YOSELF a poncho fool!
