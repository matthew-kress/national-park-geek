<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>National Park Weather Service</title>
    <c:url value="npgeek.css" var="cssHref" />
    <link rel="stylesheet" href="${cssHref}">
</head>

<body>
    <header>
    		<c:url value="/" var="homePageHref" />
    		<c:url value="/img/logo.png" var="logoSrc" />
        <a href="${homePageHref}">
        		<img src="${logoSrc}" alt="National Parks logo" />
        </a>
        <h1 id="nationalPark">National Parks</h1>
    </header>
    		<h4 id="trippy">We trippy mane</h4>
    <nav>
    	<div id="homeDailySurvey">
        <h2>Take our Daily Survey</h2>
        <p>Take the Daily Survey 7 days in a row and get a free 3 Bean Burrito</p>
        </div>
        <ul>
        		<c:url var="dailySurvey" value="/surveyInput"/>
            <li id="surveyLink"><a href="${dailySurvey}">Daily Survey</a></li>
            
            <c:url var="homePage" value="/"/>
            <li><a href="${homePage}">Home</a></li>
        </ul>
    </nav>