<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp"/>

<div id="main-content">
	<h2>Daily Survey</h2>
<form method="POST" action="surveyInput">
	<div class="formInputGroup">
		<label for="park">Choose Your Favorite Park</label> 
		<select name="parkCode"	id="parkName">
			<c:forEach var="parkChoice" items="${park}">
			<option value="${parkChoice.parkCode}">"${parkChoice.parkName}"</option>
			</c:forEach>
		</select>
	</div>
	<div class="formInputGroup">
		<label for="email">Email Address</label> 
		<input type="text" name="emailAddress" id="emailAddress"/>
	</div>
	<div class="formInputGroup">
		<label for="state">State of Residence</label> 
		<input type="text" name="state" id="state"/>
	</div>
	<div class="formInputGroup">
		<label for="activityLevel">Choose Your Activity Level</label> 
		<select name="activityLevel"	id="activityLevel">
			<option value="inactive">Inactive</option>
			<option value="sedentary">Sedentary</option>
			<option value="active">Active</option>
			<option value="extremelyActive">Extremely Active</option>
		</select>
	</div>
	<input class="formSubmitButton" type="submit" value="Submit" />
</form>
</div>

<c:import url="/WEB-INF/jsp/common/footer.jsp"/>