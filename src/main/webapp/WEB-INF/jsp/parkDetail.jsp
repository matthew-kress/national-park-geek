<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<c:url var="parkDetailPic" value="/img/parks/${detailInfo.parkCode.toLowerCase()}.jpg" />
<img class="parkDetailPic" src="${parkDetailPic}">
<div id="detailInfo">
<span id="parkDetailTitle" class="detailTitles"><i><c:out value="${detailInfo.parkName}" /></i></span>
<br>
<c:out value="${detailInfo.state}" /><br>
<b><span class="detailTitles">Number of Acres: </span></b><span id="acres" ><c:out value="${detailInfo.acreage}" /></span><br>
<b><span class="detailTitles">Elevation(ft.): </span></b><span id ="elevation"><c:out value="${detailInfo.elevationInFeet}" /></span><br>
<b><span class="detailTitles">Miles of Trail: </span></b><span id="milesOfTrail" ><c:out value="${detailInfo.milesOfTrail}" /></span><br>
<b><span class="detailTitles">Number of Campsites: </span></b><span id="campsites" ><c:out value="${detailInfo.numberOfCampsites}" /></span><br>
<b><span class="detailTitles">Climate: </span></b><span id="climate"><c:out value="${detailInfo.climate}" /></span><br>
<b><span class="detailTitles">Year Founded: </span></b><span id="year"><c:out value="${detailInfo.yearFounded}" /></span><br>
<b><span class="detailTitles">Annual Visitor Count: </span></b><span id="visitorCount"><c:out value="${detailInfo.annualVisitorCount}"/></span><br>
<b><span class="detailTitles">Quote: </span></b><span id="quote" ><c:out value="${detailInfo.inspirationalQuote}" /></span>
- <span id="quoteSource" ><c:out value="${detailInfo.inspirationalQuoteSource}" /></span><br>
<b><span class="detailTitles">Description: </span></b><span id="description"><c:out value="${detailInfo.parkDescription}" /></span><br>
<b><span class="detailTitles">Entry Fee: </span></b>$<span id="entryFee"><c:out value="${detailInfo.entryFee}" /></span><br>
<b><span class="detailTitles">Number of Animal Species: </span></b><span id="numberOfAnimals" ><c:out value="${detailInfo.numberOfAnimalSpecies}" /></span><br>
</div>


<h1 id="weatherTitle">Weather -5 day Forecast</h1>
<form method="GET" action="parkDetail">
	<input type="hidden" name="parkCode" value="${param.parkCode}"/>
<c:choose>
<c:when test="${tempUnit == 'Fahrenheit'}">
	<input type="hidden" name="tempUnit" value="Celsius"/>
<c:set var="unitName" value="Celsius"/>
</c:when>
<c:otherwise>
	<input type="hidden" name="tempUnit" value="Fahrenheit"/>
<c:set var="unitName" value="Fahrenheit"/>
</c:otherwise>
</c:choose>

<input class="formSubmitButton" type="submit" value="Change to ${unitName}" />
</form>
<c:forEach var="dayOfWeather" items="${weather}">
<div class="weather">
<c:url var="weatherPic" value="/img/weather/${dayOfWeather.forecast}.png" />
<img class="weatherPic" src="${weatherPic}"><br>
<u>Day <span id="fiveDayForecast"><c:out value="${dayOfWeather.fiveDayForecastValue}"/></span></u><br>
<c:choose>
<c:when test="${tempUnit == 'Fahrenheit'}">
Low: <span id= "low"><c:out value="${dayOfWeather.low}"/>F</span><br>
High: <span id ="high"><c:out value="${dayOfWeather.high}"/>F</span><br>
</c:when>
<c:otherwise>
Low: <span id= "low"><c:out value="${dayOfWeather.lowC}"/>C</span><br>
High: <span id ="high"><c:out value="${dayOfWeather.highC}"/>C</span><br>
</c:otherwise>

</c:choose>
Forecast Recommendation: <span id="recommendation"><c:out value="${dayOfWeather.forecastRecommendations}"/></span><br>
Temperature Recommendation: <span id="tempRecommendation"><c:out value="${dayOfWeather.temperatureRecommendations }"/></span>
</div>
</c:forEach>




<c:import url="/WEB-INF/jsp/common/footer.jsp" />