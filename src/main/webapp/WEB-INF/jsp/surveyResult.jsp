<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp"/>

<div id="main-content">
<div id="surveyResult">
	<h1>Survey Results</h1>
	<h5>The most popular park is <c:out value="${surveys.parkName}"></c:out></h5>
	</div>
</div>
<c:import url="/WEB-INF/jsp/common/footer.jsp"/>