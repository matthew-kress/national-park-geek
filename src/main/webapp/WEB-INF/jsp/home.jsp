<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<div id="homeBody">
<c:forEach var="info" items="${park}">
	<c:url var="parkPic" value="img/parks/${info.parkCode}.jpg" />
	<c:url var="parkDetail" value="/parkDetail">
		<c:param name="parkCode" value="${info.parkCode}" />
	</c:url>
	<a href="${parkDetail}"><img id="${info.parkCode}" class="parkPic" src="${parkPic}"></a>
	<div class="homeInfo">
	<span class="parkName"><c:out value="${info.parkName}" /></span>
	<span class="parkState"><c:out value="${info.state}" /></span>
	<span class="parkDescription">
	<c:out value="${info.parkDescription}" /></span>
	</div>
</c:forEach>
</div>
<c:import url="/WEB-INF/jsp/common/footer.jsp" />