package com.techelevator.dao;

import java.util.List;

import com.techelevator.npgeek.Weather;

public interface WeatherDAO {
	public List<Weather> getAllWeather(String parkCode);
}
