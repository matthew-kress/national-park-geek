package com.techelevator.dao;

import java.util.List;

import com.techelevator.npgeek.Survey;
import com.techelevator.park.Park;

public interface SurveyDAO {

	public List<Survey> getSurveys();
	
	public void postSurvey(Survey survey);
	
	public Park getLeadingPark();
}
