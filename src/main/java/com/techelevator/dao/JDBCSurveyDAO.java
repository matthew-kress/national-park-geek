package com.techelevator.dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.npgeek.Survey;
import com.techelevator.park.Park;

@Component
public class JDBCSurveyDAO implements SurveyDAO {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JDBCSurveyDAO(DataSource datasource) {
		this.jdbcTemplate = new JdbcTemplate(datasource);
	}
	
	@Autowired
	ParkDAO parkDao;
	SurveyDAO surveyDao;

	@Override
	public List<Survey> getSurveys() {
		List<Survey> allSurveys = new ArrayList<>();
		String sqlSelectAllSurveys = "SELECT * FROM survey_result";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllSurveys);
		while (results.next()) {
			Survey survey = new Survey();
			survey.setEmailAddress(results.getString("emailAddress"));
			survey.setState(results.getString("state"));
			survey.setActivityLevel(results.getString("activityLevel"));

			allSurveys.add(survey);
		}
		return allSurveys;
	}

	@Override
	public void postSurvey(Survey survey) {
		int id = getNextId();
		String sqlInsertSurvey = "INSERT INTO survey_result(surveyId, parkCode, emailAddress, state, activityLevel) VALUES (?,?,?,?,?)";
		jdbcTemplate.update(sqlInsertSurvey, id, survey.getParkCode(), survey.getEmailAddress(), survey.getState(),
				survey.getActivityLevel());
		survey.setSurveyId(id);

	}
	
	

	private int getNextId() {
		String sqlSelectNextId = "SELECT NEXTVAL('seq_surveyId')";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectNextId);
		int id = 0;
		if(results.next()){
			id = results.getInt(1);
		} else {
			throw new RuntimeException("Something happened bruh");
		}
		return id;
	}

	@Override
	public Park getLeadingPark() {
		Park park = new Park();
		String sqlSelectLeadingSurvey = "SELECT park.parkCode, park.parkName, COUNT(*) FROM survey_result "
										+"JOIN park ON park.parkCode = UPPER(survey_result.parkCode) "
										+"GROUP BY park.parkCode "
										+"ORDER BY COUNT desc "
										+"LIMIT 1";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectLeadingSurvey);
		while(results.next()){
			park.setParkName(results.getString("parkName"));
		}
		return park;
	}
}
