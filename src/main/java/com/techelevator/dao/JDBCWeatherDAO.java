package com.techelevator.dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.npgeek.Weather;
import com.techelevator.park.Park;

@Component
public class JDBCWeatherDAO implements WeatherDAO {
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JDBCWeatherDAO(DataSource datasource) {
		this.jdbcTemplate = new JdbcTemplate(datasource);
	}

	@Override
	public List<Weather> getAllWeather(String parkCode) {
		List<Weather> allWeather = new ArrayList<>();
		String sqlSelectAllWeather = "SELECT * FROM weather WHERE parkCode = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllWeather, parkCode.toUpperCase());
		while (results.next()) {
			Weather weather = new Weather(sqlSelectAllWeather);
			weather.setFiveDayForecastValue(results.getInt("fiveDayForecastValue"));
			weather.setLow(results.getInt("low"));
			weather.setHigh(results.getInt("high"));
			weather.setForecast(results.getString("forecast"));
			allWeather.add(weather);
		}
		return allWeather;
	}
}
