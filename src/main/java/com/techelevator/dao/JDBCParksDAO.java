package com.techelevator.dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.park.Park;

@Component
public class JDBCParksDAO implements ParkDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JDBCParksDAO (DataSource datasource) {
		this.jdbcTemplate = new JdbcTemplate(datasource);
	}
	
	@Override
	public List<Park> getAllParksHomepage() {
		List<Park> allParks = new ArrayList<>();
		String sqlSelectAllParks = "SELECT * FROM park";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllParks);
		while(results.next()) {
			Park park = new Park();
				park.setParkName(results.getString("parkName"));
				park.setState(results.getString("state"));
				park.setParkDescription(results.getString("parkDescription"));
				allParks.add(park);
		}
	return allParks;
		
				
	}

	@Override
	public List<Park> getAllParks() {
		List<Park> allParks = new ArrayList<>();
		String sqlSelectAllParks = "SELECT * FROM park";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllParks);
		while(results.next()) {
			allParks.add(mapToRowToPark(results));
		}
	return allParks;
		
	}

	@Override
	public Park getParkByParkCode(String parkCode) {
		String sqlSelectParkByParkCode = "SELECT * FROM park WHERE parkCode= ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectParkByParkCode, parkCode.toUpperCase());
		while(results.next()) {
			return mapToRowToPark(results);
		}
		return null;
	}
	private Park mapToRowToPark (SqlRowSet row) {
		Park park = new Park();
		park.setParkCode(row.getString("parkCode").toLowerCase());
		park.setParkName(row.getString("parkName"));
		park.setState(row.getString("state"));
		park.setParkDescription(row.getString("parkDescription"));
		park.setAcreage(row.getInt("acreage"));
		park.setElevationInFeet(row.getInt("elevationInFeet"));
		park.setMilesOfTrail(row.getDouble("milesOfTrail"));
		park.setNumberOfCampsites(row.getInt("numberOfCampsites"));
		park.setClimate(row.getString("climate"));
		park.setYearFounded(row.getInt("yearFounded"));
		park.setAnnualVisitorCount(row.getInt("annualVisitorCount"));
		park.setInspirationalQuote(row.getString("inspirationalQuote"));
		park.setInspirationalQuoteSource(row.getString("inspirationalQuoteSource"));
		park.setEntryFee(row.getInt("entryFee"));
		park.setNumberOfAnimalSpecies(row.getInt("numberOfAnimalSpecies"));
		return park;
		
	}

}
