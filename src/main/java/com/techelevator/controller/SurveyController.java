package com.techelevator.controller;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.techelevator.dao.ParkDAO;
import com.techelevator.dao.SurveyDAO;
import com.techelevator.npgeek.Survey;

@Controller
public class SurveyController {
	
	@Autowired
	SurveyDAO dao;
	
	@Autowired
	ParkDAO parkDao;

	@RequestMapping(path="/surveyInput", method=RequestMethod.GET)
	public String showSurveyInputPage(HttpServletRequest request) {
		// Get the parks
		// put the parks on the request
//		parkDao.getAllParks();
		request.setAttribute("park", parkDao.getAllParks());
		return "surveyInput";
	}
	
	@RequestMapping(path="/surveyInput", method=RequestMethod.POST)
	public String processSurveyInputPage(Survey survey) {
		dao.postSurvey(survey);
		return "redirect:/surveyResult";
	}
	
	@RequestMapping("/surveyResult")
	public String getSurveyInputPage(HttpServletRequest request) {
		request.setAttribute("surveys",dao.getLeadingPark());
		return "surveyResult";
	}
}
