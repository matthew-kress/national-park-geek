package com.techelevator.controller;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.techelevator.dao.JDBCParksDAO;
import com.techelevator.dao.ParkDAO;

@Controller
public class HomeController {

	@Autowired
	ParkDAO parkDao;
	
	@RequestMapping("/")
	public String displayHomePage(HttpServletRequest request) {
		request.setAttribute("park", parkDao.getAllParks());
		
		return "home";
	}
}
