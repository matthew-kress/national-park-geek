package com.techelevator.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.dao.ParkDAO;
import com.techelevator.dao.WeatherDAO;
import com.techelevator.npgeek.Weather;
import com.techelevator.park.Park;



@Controller
@SessionAttributes("tempUnit")
public class WeatherController {
	
	@Autowired
	ParkDAO parkDao;
	@Autowired
	WeatherDAO weatherDao;
	
	@RequestMapping("/parkDetail")
	public String showParkDetail(HttpServletRequest request, ModelMap map) {
		String parkCode = request.getParameter("parkCode");
		Park selectedPark = parkDao.getParkByParkCode(parkCode);
		request.setAttribute("detailInfo", selectedPark);
		List<Weather> weather = weatherDao.getAllWeather(parkCode);
		request.setAttribute("weather", weather);
		
		String tempUnit = request.getParameter("tempUnit");
		if(tempUnit == null){
			if(map.containsKey("tempUnit")){
				tempUnit = (String) map.get("tempUnit");
			}
			else{
				tempUnit = "Fahrenheit";
			}
		}
		map.put("tempUnit", tempUnit);
	return "parkDetail";
	}
	
	
}
