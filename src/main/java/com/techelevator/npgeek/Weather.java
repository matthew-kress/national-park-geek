package com.techelevator.npgeek;

import com.techelevator.park.Park;

public class Weather {
	private String parkCode;
	private int fiveDayForecastValue;
	private int low;
	private int high;
	private String forecast;
	private String forecastRecommendation;
	private String temperatureRecommendation;
	
	public Weather(String parkCode) {
		this.parkCode = parkCode;
	}
	
	public String getParkCode() {
		return parkCode;
	}
	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}
	public int getFiveDayForecastValue() {
		return fiveDayForecastValue;
	}
	public void setFiveDayForecastValue(int fiveDayForecastValue) {
		this.fiveDayForecastValue = fiveDayForecastValue;
	}
	public int getLow() {
		return low;
	}
	
	public int getLowC() {
		low = (int)((low -32) * (5/9.0));
		return low;
	}
	public void setLow(int low) {
		this.low = low;
	}
	public int getHigh() {
		return high;
	}
	public int getHighC(){
		high = (int)((high - 32) * (5/9.0));
		return high;
	}

	public void setHigh(int high) {
		this.high = high;
	}
	public String getForecast() {
		return forecast;
	}
	public void setForecast(String forecast) {
		this.forecast = forecast;
	}
	
	public String getForecastRecommendations(){
		Park park = new Park();
		String parkCode = park.getParkCode();
		String recommendation = "";
		if(getForecast().equals("snow")) {
			recommendation = "Remember to pack hockey skates!";
		}
		else if(getForecast().equals("rain")) {
			recommendation = "Get YOSELF a poncho fool!";
		}
		else if(getForecast().equals("thunderstorms")) {
			recommendation = "It gonna storm, grab a golf club, hide under a tree";
		}
		else if(getForecast().equals("sunny")) {
			recommendation = "Bring some shades bruh";
		}
		else {
			recommendation = "The weather aight";
		}
		return recommendation;
	}
	
	public String getTemperatureRecommendations(){
		Park park = new Park();
		String parkCode = park.getParkCode();
		String recommendation = "";
		if(getHigh() > 75){
			recommendation = "It gonna be hot bring extra water my dood";
		}
		else if((getHigh()-getLow()) > 20) {
			recommendation = "Get your underarmour on, the temp be all over the place";
		}
		else if (getLow() < 20) {
			recommendation = "It's colder then your ex-girlfriends heart";
		}
		else {
			recommendation = "Enjoy the normal weather";
		}
		return recommendation;
	}
	
	
	
}
